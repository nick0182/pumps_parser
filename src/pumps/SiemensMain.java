package pumps;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.*;

public class SiemensMain {

    private static String from = "/home/nikolay/Documents/Other/hpp2018/Siemens";

    private static File fileFrom = new File(from);

    private static File[] files = Optional.ofNullable(fileFrom.listFiles()).orElseThrow(NoClassDefFoundError::new);

    private static FileWriter writer;

    private static final String PULSE_FILE_NAME = "PulseFileName";

    private static final String SERIAL_NUMBER = "SerialNumber";

    private static final String MODEL = "Model";

    private static final String PRESSURE_LOST_TEST = "PressureLostTest";

    private static final String RELEASE = "Release";

    private static final String MANUFACTURER = "Manufacturer";

    private static final String PUMP_CODE = "PumpCode";

    private static final String MAX_REQUIRED_PRESSURE = "MaxRequiredPressure";

    private static final String PUMP_TYPE = "PumpType";

    private static final String N1 = "N1";

    private static final String N2 = "N2";

    private static final String N3 = "N3";

    private static final String N4 = "N4";

    private static final String N5 = "N5";

    private static final String N6 = "N6";

    private static final String N7 = "N7";

    private static final String N8 = "N8";

    private static final String N9 = "N9";

    private static final String N10 = "N10";

    private static final String PUMP_EQUIPMENT = "PumpEquipment";

    private static final String PRESSURE_CONTROL = "PressureControl";

    private static final String PUMP_DIRECTION = "PumpDirection";

    private static final String WORKING_TEMPERATURE = "WorkingTemperature";

    private static final String INNER_VOLUME = "InnerVolume";

    private static final String RPO_TABLE = "RPO_Table";

    private static final String REGULATOR_TYPE = "RegulatorType";

    private static final String I_REGULATOR_CURRENT_FOR_INJ_TESTS = "I_RegulatorCurrentForInjTests";

    private static final String MINUMUM_REGULATOR_CURRENT = "MinimumRegulatorCurrent";

    private static final String MAXIMUM_REGULATOR_CURRENT = "MaximumRegulatorCurrent";

    private static final List<Map<String, String>> RESULT_LIST = new LinkedList<>();

    static {

        for (File file : files) {

            Map<String, String> fileMap = new LinkedHashMap<>();

            fileMap.put(PULSE_FILE_NAME, "");
            fileMap.put(SERIAL_NUMBER, "");
            fileMap.put(MODEL, "");
            fileMap.put(PRESSURE_LOST_TEST, "");
            fileMap.put(RELEASE, "");
            fileMap.put(MANUFACTURER, "");
            fileMap.put(PUMP_CODE, "");
            fileMap.put(MAX_REQUIRED_PRESSURE, "");
            fileMap.put(PUMP_TYPE, "");
            fileMap.put(N1, "");
            fileMap.put(N2, "");
            fileMap.put(N3, "");
            fileMap.put(N4, "");
            fileMap.put(N5, "");
            fileMap.put(N6, "");
            fileMap.put(N7, "");
            fileMap.put(N8, "");
            fileMap.put(N9, "");
            fileMap.put(N10, "");
            fileMap.put(PUMP_EQUIPMENT, "");
            fileMap.put(PRESSURE_CONTROL, "");
            fileMap.put(PUMP_DIRECTION, "");
            fileMap.put(WORKING_TEMPERATURE, "");
            fileMap.put(INNER_VOLUME, "");
            fileMap.put(RPO_TABLE, "");
            fileMap.put(REGULATOR_TYPE, "");
            fileMap.put(I_REGULATOR_CURRENT_FOR_INJ_TESTS, "");
            fileMap.put(MINUMUM_REGULATOR_CURRENT, "");
            fileMap.put(MAXIMUM_REGULATOR_CURRENT, "");

            RESULT_LIST.add(fileMap);

        }


    }

    public static void main(String[] args) {

        try {
            parse();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void parse() throws IOException {

        String to = "/home/nikolay/Documents/Other/hpp2018/target/Siemens";

        writer = new FileWriter(to + File.separator + "siemens.csv", true);

        writer.append(PULSE_FILE_NAME).append(',')
                .append(SERIAL_NUMBER).append(',')
                .append(MODEL).append(',')
                .append(PRESSURE_LOST_TEST).append(',')
                .append(RELEASE).append(',')
                .append(MANUFACTURER).append(',')
                .append(PUMP_CODE).append(',')
                .append(MAX_REQUIRED_PRESSURE).append(',')
                .append(PUMP_TYPE).append(',')
                .append(N1).append(',')
                .append(N2).append(',')
                .append(N3).append(',')
                .append(N4).append(',')
                .append(N5).append(',')
                .append(N6).append(',')
                .append(N7).append(',')
                .append(N8).append(',')
                .append(N9).append(',')
                .append(N10).append(',')
                .append(PUMP_EQUIPMENT).append(',')
                .append(PRESSURE_CONTROL).append(',')
                .append(PUMP_DIRECTION).append(',')
                .append(WORKING_TEMPERATURE).append(',')
                .append(INNER_VOLUME).append(',')
                .append(RPO_TABLE).append(',')
                .append(REGULATOR_TYPE).append(',')
                .append(I_REGULATOR_CURRENT_FOR_INJ_TESTS).append(',')
                .append(MINUMUM_REGULATOR_CURRENT).append(',')
                .append(MAXIMUM_REGULATOR_CURRENT).append('\n');

        readFile();

        writeFile();

        writer.flush();
        writer.close();

    }

    private static void readFile() throws IOException {

        for (int i = 0; i < files.length; i++) {
            int index = i;
            Files
                    .lines(files[i].toPath(), StandardCharsets.UTF_8).filter(line ->
                    line.contains(PULSE_FILE_NAME)
                            || line.contains(SERIAL_NUMBER)
                            || line.contains(MODEL)
                            || line.contains(PRESSURE_LOST_TEST)
                            || line.contains(RELEASE)
                            || line.contains(MANUFACTURER)
                            || line.contains(PUMP_CODE)
                            || line.contains(MAX_REQUIRED_PRESSURE)
                            || line.contains(PUMP_TYPE)
                            || line.contains(N1)
                            || line.contains(N2)
                            || line.contains(N3)
                            || line.contains(N4)
                            || line.contains(N5)
                            || line.contains(N6)
                            || line.contains(N7)
                            || line.contains(N8)
                            || line.contains(N9)
                            || line.contains(N10)
                            || line.contains(PUMP_EQUIPMENT)
                            || line.contains(PRESSURE_CONTROL)
                            || line.contains(PUMP_DIRECTION)
                            || line.contains(WORKING_TEMPERATURE)
                            || line.contains(INNER_VOLUME)
                            || line.contains(RPO_TABLE)
                            || line.contains(REGULATOR_TYPE)
                            || line.contains(I_REGULATOR_CURRENT_FOR_INJ_TESTS)
                            || line.contains(MINUMUM_REGULATOR_CURRENT)
                            || line.contains(MAXIMUM_REGULATOR_CURRENT))
                    .forEach(line1 -> parseLine(line1, index));
        }


    }

    private static void writeFile() throws IOException {

        for (int i = 0; i < files.length; i++)
            writer.append(RESULT_LIST.get(i).get(PULSE_FILE_NAME)).append(',')
                    .append(RESULT_LIST.get(i).get(SERIAL_NUMBER)).append(',')
                    .append(RESULT_LIST.get(i).get(MODEL)).append(',')
                    .append(RESULT_LIST.get(i).get(PRESSURE_LOST_TEST)).append(',')
                    .append(RESULT_LIST.get(i).get(RELEASE)).append(',')
                    .append(RESULT_LIST.get(i).get(MANUFACTURER)).append(',')
                    .append(RESULT_LIST.get(i).get(PUMP_CODE)).append(',')
                    .append(RESULT_LIST.get(i).get(MAX_REQUIRED_PRESSURE)).append(',')
                    .append(RESULT_LIST.get(i).get(PUMP_TYPE)).append(',')
                    .append(RESULT_LIST.get(i).get(N1)).append(',')
                    .append(RESULT_LIST.get(i).get(N2)).append(',')
                    .append(RESULT_LIST.get(i).get(N3)).append(',')
                    .append(RESULT_LIST.get(i).get(N4)).append(',')
                    .append(RESULT_LIST.get(i).get(N5)).append(',')
                    .append(RESULT_LIST.get(i).get(N6)).append(',')
                    .append(RESULT_LIST.get(i).get(N7)).append(',')
                    .append(RESULT_LIST.get(i).get(N8)).append(',')
                    .append(RESULT_LIST.get(i).get(N9)).append(',')
                    .append(RESULT_LIST.get(i).get(N10)).append(',')
                    .append(RESULT_LIST.get(i).get(PUMP_EQUIPMENT)).append(',')
                    .append(RESULT_LIST.get(i).get(PRESSURE_CONTROL)).append(',')
                    .append(RESULT_LIST.get(i).get(PUMP_DIRECTION)).append(',')
                    .append(RESULT_LIST.get(i).get(WORKING_TEMPERATURE)).append(',')
                    .append(RESULT_LIST.get(i).get(INNER_VOLUME)).append(',')
                    .append(RESULT_LIST.get(i).get(RPO_TABLE)).append(',')
                    .append(RESULT_LIST.get(i).get(REGULATOR_TYPE)).append(',')
                    .append(RESULT_LIST.get(i).get(I_REGULATOR_CURRENT_FOR_INJ_TESTS)).append(',')
                    .append(RESULT_LIST.get(i).get(MINUMUM_REGULATOR_CURRENT)).append(',')
                    .append(RESULT_LIST.get(i).get(MAXIMUM_REGULATOR_CURRENT)).append('\n');

    }

    private static void parseLine(String line, int index) {

        String[] splitter = line.split(":");

        if (splitter.length > 1) {
            String trimmed1 = splitter[0].trim();
            String trimmed2 = splitter[1].trim();
            RESULT_LIST.get(index).put(trimmed1, trimmed2);
        }


    }
}
