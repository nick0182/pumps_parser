package pumps_tests;

import javafx.util.Pair;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

public class BoschMain {

    private static String from = "/home/nikolay/Documents/Other/hpp2018/Bosch";

    private static File fileFrom = new File(from);

    private static File[] files = Optional.ofNullable(fileFrom.listFiles()).orElseThrow(NoClassDefFoundError::new);

    private static FileWriter writer;

    private static final String PUMP_CODE = "PumpCode";

    private static final String TEST_NAME = "TEST_NAME";

    private static final String TEST = "TEST";

    private static final String VACUUM = "Vacuum";

    private static final String ADJUSTING_TIME = "AdjustingTime";

    private static final String TEST_TIME = "TestTime";

    private static final String MOTOR_SPEED = "MotorSpeed";

    private static final String SETTED_PRESSURE = "SettedPressure";

    private static final String MIN_PRESSURE = "MinPressure";

    private static final String MIN_FLOW = "MinFlow";

    private static final String MAX_FLOW = "MaxFlow";

    private static final String MIN_BACK_FLOW = "MinBackFlow";

    private static final String MAX_BACK_FLOW = "MaxBackFlow";

    private static final String I_REGULATOR_CURRENT = "I_RegulatorCurrent";

    private static final String SLOPE_TIME = "SlopeTime";

    private static final String MEASUREMENT_TIME = "MeasurementTime";

    private static final String WARNING_PRESSURE = "WarningPressure";

    private static final String EXCLUDE = "I_RegulatorCurrentForInjTests";

    private static Map<Pair<String, Integer>, Map<String, String>> onePumpMap = new LinkedHashMap<>();

    private static String pumpCode;

    private static int currentFileNumber = 0;

    private static String currentTestName;

    private static Pair<String, Integer> currentPair;

    public static void main(String[] args) {

        try {
            parse();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                writer.flush();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private static void parse() throws IOException {

        String to = "/home/nikolay/Documents/Other/hpp2018/target/Bosch";

        writer = new FileWriter(to + File.separator + "bosch_tests.csv", true);

        writer.append(PUMP_CODE).append(',')
                .append(TEST_NAME).append(',')
                .append(TEST).append(',')
                .append(VACUUM).append(',')
                .append(ADJUSTING_TIME).append(',')
                .append(TEST_TIME).append(',')
                .append(MOTOR_SPEED).append(',')
                .append(SETTED_PRESSURE).append(',')
                .append(MIN_PRESSURE).append(',')
                .append(MIN_FLOW).append(',')
                .append(MAX_FLOW).append(',')
                .append(MIN_BACK_FLOW).append(',')
                .append(MAX_BACK_FLOW).append(',')
                .append(I_REGULATOR_CURRENT).append(',')
                .append(SLOPE_TIME).append(',')
                .append(MEASUREMENT_TIME).append(',')
                .append(WARNING_PRESSURE).append('\n');

        readFile();

    }

    private static void readFile() throws IOException {

        for (int i = 0; i < files.length; i++) {
            int fileNumber = i;
            Files
                    .lines(files[i].toPath(), StandardCharsets.UTF_8).filter(line -> line.contains(PUMP_CODE)
                    || line.contains(TEST_NAME)
                    || line.contains(TEST)
                    || line.contains(VACUUM)
                    || line.contains(ADJUSTING_TIME)
                    || line.contains(TEST_TIME)
                    || line.contains(MOTOR_SPEED)
                    || line.contains(SETTED_PRESSURE)
                    || line.contains(MIN_PRESSURE)
                    || line.contains(MIN_FLOW)
                    || line.contains(MAX_FLOW)
                    || line.contains(MIN_BACK_FLOW)
                    || line.contains(MAX_BACK_FLOW)
                    || (line.contains(I_REGULATOR_CURRENT) && !line.contains(EXCLUDE))
                    || line.contains(SLOPE_TIME)
                    || line.contains(MEASUREMENT_TIME)
                    || line.contains(WARNING_PRESSURE))
                    .forEach(line -> parse(line, fileNumber));
        }

    }

    private static void parse(String line, int fileNumber) {

        String[] splitter = line.split(":");

        String trimmed1 = splitter[0].trim();

        String trimmed2 = splitter[1].trim();

        if (fileNumber == currentFileNumber) {

            switch (trimmed1) {
                case PUMP_CODE:
                    pumpCode = trimmed2;
                    break;
                case TEST_NAME:
                    currentTestName = trimmed2;
                    currentPair = new Pair<>(currentTestName, 1);
                    onePumpMap.put(currentPair, getEmptyValuesMap());
                    break;
                case TEST:
                    int currentTestNumber = Integer.valueOf(splitter[1].trim());
                    if (currentTestNumber > 1) {
                        currentPair = new Pair<>(currentTestName, currentTestNumber);
                        onePumpMap.put(currentPair, getEmptyValuesMap());
                    }
                    break;
                case VACUUM:
                case ADJUSTING_TIME:
                case TEST_TIME:
                case MOTOR_SPEED:
                case SETTED_PRESSURE:
                case MIN_PRESSURE:
                case MIN_FLOW:
                case MAX_FLOW:
                case MIN_BACK_FLOW:
                case MAX_BACK_FLOW:
                case I_REGULATOR_CURRENT:
                case SLOPE_TIME:
                case MEASUREMENT_TIME:
                case WARNING_PRESSURE:
                    onePumpMap.get(currentPair).put(trimmed1, trimmed2);
                    break;
            }

        } else {

            currentFileNumber = fileNumber;

            try {
                writePump();
                if (trimmed1.equals(PUMP_CODE))
                    pumpCode = trimmed2;
                onePumpMap = new LinkedHashMap<>();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    private static void writePump() throws IOException {

        for (Map.Entry<Pair<String, Integer>, Map<String, String>> line : onePumpMap.entrySet()) {

            writer.append(pumpCode).append(',')
                    .append(line.getKey().getKey()).append(',')
                    .append(line.getKey().getValue().toString()).append(',')
                    .append(line.getValue().get(VACUUM)).append(',')
                    .append(line.getValue().get(ADJUSTING_TIME)).append(',')
                    .append(line.getValue().get(TEST_TIME)).append(',')
                    .append(line.getValue().get(MOTOR_SPEED)).append(',')
                    .append(line.getValue().get(SETTED_PRESSURE)).append(',')
                    .append(line.getValue().get(MIN_PRESSURE)).append(',')
                    .append(line.getValue().get(MIN_FLOW)).append(',')
                    .append(line.getValue().get(MAX_FLOW)).append(',')
                    .append(line.getValue().get(MIN_BACK_FLOW)).append(',')
                    .append(line.getValue().get(MAX_BACK_FLOW)).append(',')
                    .append(line.getValue().get(I_REGULATOR_CURRENT)).append(',')
                    .append(line.getValue().get(SLOPE_TIME)).append(',')
                    .append(line.getValue().get(MEASUREMENT_TIME)).append(',')
                    .append(line.getValue().get(WARNING_PRESSURE)).append('\n');

        }

    }

    private static Map<String, String> getEmptyValuesMap() {

        Map<String, String> emptyValuesMap = new LinkedHashMap<>();

        emptyValuesMap.put(VACUUM, "");
        emptyValuesMap.put(ADJUSTING_TIME, "");
        emptyValuesMap.put(TEST_TIME, "");
        emptyValuesMap.put(MOTOR_SPEED, "");
        emptyValuesMap.put(SETTED_PRESSURE, "");
        emptyValuesMap.put(MIN_PRESSURE, "");
        emptyValuesMap.put(MIN_FLOW, "");
        emptyValuesMap.put(MAX_FLOW, "");
        emptyValuesMap.put(MIN_BACK_FLOW, "");
        emptyValuesMap.put(MAX_BACK_FLOW, "");
        emptyValuesMap.put(I_REGULATOR_CURRENT, "");
        emptyValuesMap.put(SLOPE_TIME, "");
        emptyValuesMap.put(MEASUREMENT_TIME, "");
        emptyValuesMap.put(WARNING_PRESSURE, "");

        return emptyValuesMap;

    }


}
